<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfidProduct extends Model
{
	protected $table = 'rfid_products';
	protected $fillable = ['rfid_id','product_id'];

	public function getProduct(){
		return $this->hasOne('App\Products','id','product_id');
	}
}
