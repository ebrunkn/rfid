<?php

namespace App\Http\Controllers;

use App\Rfid;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	public function rfid(Request $request){
		$rfid = Rfid::get();
		return response()->json($rfid);
	}
    public function product(Request $request, $rfid){

		$rfid = Rfid::where('rfid',$rfid)->first();

		if($rfid){
			if($rfid->getProductRel()->count()){
				return response()->json([
					'rfid'=>$rfid['rfid'],
					'item'=>$rfid->getProductRel->getProduct['name'],
					'desc'=>$rfid->getProductRel->getProduct['description'],
					'brand'=>$rfid->getProductRel->getProduct->getBrand['brand_name'],
					'brand_desc'=>$rfid->getProductRel->getProduct->getBrand['brand_description'],
					'avail_colors'=>$rfid->getProductRel->getProduct->getColors,
					'avail_sizes'=>$rfid->getProductRel->getProduct->getSizes,
				]);
			}else{
				return response()->json('Not related with any product');
			}
		}else{
			return response()->json('Not in record');
		}

	}
}
