<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttributes extends Model
{
	protected $table = 'product_attributes';
	protected $fillable = ['product_id','attr','value'];

    public function scopeColors($query){
		return $query->where('attr','color')->select('value as color');
	}

	public function scopeSizes($query){
		return $query->where('attr','size')->select('value as size');
	}

}
