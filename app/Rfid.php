<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rfid extends Model
{
	protected $table = 'rfids';
	protected $fillable = ['rfid'];

	public function getProductRel(){
		return $this->hasOne('App\RfidProduct','rfid_id','id');
	}
}
