<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
	protected $table = 'products';
	protected $fillable = ['name','description','brand_id'];

	public function getBrand(){
		return $this->hasOne('App\Brands','id','brand_id');
	}
	public function getColors(){
		return $this->hasMany('App\ProductAttributes','product_id','id')->colors();
	}
	public function getSizes(){
		return $this->hasMany('App\ProductAttributes','product_id','id')->sizes();
	}


}
