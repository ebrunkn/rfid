<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfidProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfid_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rfid_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->timestamps();

			$table->foreign('rfid_id')->references('id')->on('rfids');
			$table->foreign('product_id')->references('id')->on('products');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('rfid_products', function(Blueprint $table){
			$table->dropForeign('rfid_products_rfid_id_foreign');
			$table->dropColumn('rfid_id');
			$table->dropForeign('rfid_products_product_id_foreign');
			$table->dropColumn('product_id');
		});
        Schema::dropIfExists('rfid_products');
    }
}
