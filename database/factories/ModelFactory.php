<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Categories::class, function (Faker\Generator $faker) {
	return [
		'category' => $faker->name,
	];
});

$factory->define(App\Brands::class, function (Faker\Generator $faker) {
	return [
		'brand_name' => $faker->name,
		'brand_description' => $faker->text(500),
	];
});

$factory->define(App\Products::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->name,
		'description' => $faker->text(500),
		'brand_id' => $faker->numberBetween(1,10),
	];
});

$factory->define(App\ProductAttributes::class, function (Faker\Generator $faker) {
	$attr = ['color','size'];
	$sizes = ['XS','S','M','L','XL','XLL'];
	$at = $attr[rand(0,1)];
	if($at == 'color'){
		$val =  $faker->colorName;
	}else{
		$val =  $sizes[rand(0,5)];
	}

	return [
		'product_id' => $faker->numberBetween(1,100),
		'attr' => $at,
		'value' => $val,
	];
});


$factory->define(App\Rfid::class, function (Faker\Generator $faker) {
	return [
		'rfid' => $faker->ean13,
	];
});

$factory->define(App\RfidProduct::class, function (Faker\Generator $faker) {
	return [
		'rfid_id' => $faker->numberBetween(1,100),
		'product_id' => $faker->numberBetween(1,100),
	];
});

