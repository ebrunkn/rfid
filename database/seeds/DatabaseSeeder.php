<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		$totalBrands = 10;
		factory(App\User::class, 2)->create();
		factory(App\Categories::class, 20)->create();
		factory(App\Brands::class, $totalBrands)->create();
		factory(App\Products::class, 100)->create();
		factory(App\ProductAttributes::class, 1000)->create();
		factory(App\Rfid::class, 100)->create();
		factory(App\RfidProduct::class, 100)->create();


    }
}
